/* This file is part of IntelliPort application developed by Mihai MOGA.

IntelliPort is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Open
Source Initiative, either version 3 of the License, or any later version.

IntelliPort is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
IntelliPort.  If not, see <http://www.opensource.org/licenses/gpl-3.0.html>*/

// ConfigureDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IntelliPort.h"
#include "ConfigureDlg.h"
#include "enumser.h"
#include "SerialPort.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CConfigureDlg dialog

IMPLEMENT_DYNAMIC(CConfigureDlg, CDialogEx)

CConfigureDlg::CConfigureDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CConfigureDlg::IDD, pParent)
{
}

CConfigureDlg::~CConfigureDlg()
{
}

void CConfigureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CONNECTION, m_pConnection);
	DDX_Control(pDX, IDC_NAMES, m_pSerialPortNames);
	DDX_Control(pDX, IDC_BAUD_RATE, m_pBaudRate);
	DDX_Control(pDX, IDC_DATA_BITS, m_pDataBits);
	DDX_Control(pDX, IDC_PARITY, m_pParity);
	DDX_Control(pDX, IDC_STOP_BITS, m_pStopBits);
	DDX_Control(pDX, IDC_FLOW_CONTROL, m_pFlowControl);
	DDX_Control(pDX, IDC_SOCKET_TYPE, m_pSocketType);
	DDX_Control(pDX, IDC_SERVER_IP, m_pServerIP);
	DDX_Control(pDX, IDC_SERVER_PORT, m_pServerPort);
	DDX_Control(pDX, IDC_CLIENT_IP, m_pClientIP);
	DDX_Control(pDX, IDC_CLIENT_PORT, m_pClientPort);
}

BEGIN_MESSAGE_MAP(CConfigureDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_CONNECTION, &CConfigureDlg::OnSelchangeConnection)
	ON_CBN_SELCHANGE(IDC_SOCKET_TYPE, &CConfigureDlg::OnSelchangeSocketType)
END_MESSAGE_MAP()

// CConfigureDlg message handlers
BOOL CConfigureDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_pConnection.ResetContent();
	m_pConnection.AddString(_T("Serial Port"));
	m_pConnection.AddString(_T("TCP Socket"));
	m_pConnection.AddString(_T("UDP Socket"));
	m_pConnection.SetCurSel(theApp.GetInt(_T("Connection"), 0));

	if (CEnumerateSerial::UsingRegistry(m_arrSerialPortNames))
	{
		m_pSerialPortNames.ResetContent();
		for (int nIndex = 0; nIndex < m_arrSerialPortNames.GetCount(); nIndex++)
		{
			m_pSerialPortNames.AddString(m_arrSerialPortNames.GetAt(nIndex));
		}
		m_pSerialPortNames.SetCurSel(theApp.GetInt(_T("SerialPort"), 0));
	}

	m_pBaudRate.ResetContent();
	m_pBaudRate.AddString(_T("110"));
	m_pBaudRate.AddString(_T("300"));
	m_pBaudRate.AddString(_T("600"));
	m_pBaudRate.AddString(_T("1200"));
	m_pBaudRate.AddString(_T("2400"));
	m_pBaudRate.AddString(_T("4800"));
	m_pBaudRate.AddString(_T("9600"));
	m_pBaudRate.AddString(_T("14400"));
	m_pBaudRate.AddString(_T("19200"));
	m_pBaudRate.AddString(_T("38400"));
	m_pBaudRate.AddString(_T("57600"));
	m_pBaudRate.AddString(_T("115200"));
	m_pBaudRate.AddString(_T("128000"));
	m_pBaudRate.AddString(_T("256000"));
	switch (theApp.GetInt(_T("BaudRate"), CBR_115200))
	{
		case CBR_110:
		{
			m_pBaudRate.SetCurSel(0);
			break;
		}
		case CBR_300:
		{
			m_pBaudRate.SetCurSel(1);
			break;
		}
		case CBR_600:
		{
			m_pBaudRate.SetCurSel(2);
			break;
		}
		case CBR_1200:
		{
			m_pBaudRate.SetCurSel(3);
			break;
		}
		case CBR_2400:
		{
			m_pBaudRate.SetCurSel(4);
			break;
		}
		case CBR_4800:
		{
			m_pBaudRate.SetCurSel(5);
			break;
		}
		case CBR_9600:
		{
			m_pBaudRate.SetCurSel(6);
			break;
		}
		case CBR_14400:
		{
			m_pBaudRate.SetCurSel(7);
			break;
		}
		case CBR_19200:
		{
			m_pBaudRate.SetCurSel(8);
			break;
		}
		case CBR_38400:
		{
			m_pBaudRate.SetCurSel(9);
			break;
		}
		case CBR_57600:
		{
			m_pBaudRate.SetCurSel(10);
			break;
		}
		case CBR_115200:
		default:
		{
			m_pBaudRate.SetCurSel(11);
			break;
		}
		case CBR_128000:
		{
			m_pBaudRate.SetCurSel(12);
			break;
		}
		case CBR_256000:
		{
			m_pBaudRate.SetCurSel(13);
			break;
		}
	}

	m_pDataBits.ResetContent();
	m_pDataBits.AddString(_T("7"));
	m_pDataBits.AddString(_T("8"));
	switch (theApp.GetInt(_T("DataBits"), 8))
	{
		case 7:
		{
			m_pDataBits.SetCurSel(0);
			break;
		}
		case 8:
		default:
		{
			m_pDataBits.SetCurSel(1);
			break;
		}
	}

	m_pParity.ResetContent();
	m_pParity.AddString(_T("None"));
	m_pParity.AddString(_T("Odd"));
	m_pParity.AddString(_T("Even"));
	m_pParity.AddString(_T("Mark"));
	m_pParity.AddString(_T("Space"));
	switch (theApp.GetInt(_T("Parity"), CSerialPort::NoParity))
	{
		case CSerialPort::NoParity:
		default:
		{
			m_pParity.SetCurSel(0);
			break;
		}
		case CSerialPort::OddParity:
		{
			m_pParity.SetCurSel(1);
			break;
		}
		case CSerialPort::EvenParity:
		{
			m_pParity.SetCurSel(2);
			break;
		}
		case CSerialPort::MarkParity:
		{
			m_pParity.SetCurSel(3);
			break;
		}
		case CSerialPort::SpaceParity:
		{
			m_pParity.SetCurSel(4);
			break;
		}
	}

	m_pStopBits.ResetContent();
	m_pStopBits.AddString(_T("1"));
	m_pStopBits.AddString(_T("1.5"));
	m_pStopBits.AddString(_T("2"));
	switch (theApp.GetInt(_T("StopBits"), CSerialPort::OneStopBit))
	{
		case CSerialPort::OneStopBit:
		default:
		{
			m_pStopBits.SetCurSel(0);
			break;
		}
		case CSerialPort::OnePointFiveStopBits:
		{
			m_pStopBits.SetCurSel(1);
			break;
		}
		case CSerialPort::TwoStopBits:
		{
			m_pStopBits.SetCurSel(2);
			break;
		}
	}

	m_pFlowControl.ResetContent();
	m_pFlowControl.AddString(_T("None"));
	m_pFlowControl.AddString(_T("CTS/RTS"));
	m_pFlowControl.AddString(_T("CTS/DTR"));
	m_pFlowControl.AddString(_T("DSR/RTS"));
	m_pFlowControl.AddString(_T("DSR/DTR"));
	m_pFlowControl.AddString(_T("Xon/Xoff"));
	switch (theApp.GetInt(_T("FlowControl"), CSerialPort::NoFlowControl))
	{
		case CSerialPort::NoFlowControl:
		default:
		{
			m_pFlowControl.SetCurSel(0);
			break;
		}
		case CSerialPort::CtsRtsFlowControl:
		{
			m_pFlowControl.SetCurSel(1);
			break;
		}
		case CSerialPort::CtsDtrFlowControl:
		{
			m_pFlowControl.SetCurSel(2);
			break;
		}
		case CSerialPort::DsrRtsFlowControl:
		{
			m_pFlowControl.SetCurSel(3);
			break;
		}
		case CSerialPort::DsrDtrFlowControl:
		{
			m_pFlowControl.SetCurSel(4);
			break;
		}
		case CSerialPort::XonXoffFlowControl:
		{
			m_pFlowControl.SetCurSel(5);
			break;
		}
	}

	m_pSocketType.ResetContent();
	m_pSocketType.AddString(_T("Server"));
	m_pSocketType.AddString(_T("Client"));
	m_pSocketType.SetCurSel(theApp.GetInt(_T("SocketType"), 0));

	CString strServerIP = theApp.GetString(_T("ServerIP"), _T("127.0.0.1"));
	if (strServerIP.IsEmpty()) strServerIP = _T("127.0.0.1");
	const int nServer1 = strServerIP.Find(_T('.'), 0);
	const int nServer2 = strServerIP.Find(_T('.'), nServer1 + 1);
	const int nServer3 = strServerIP.Find(_T('.'), nServer2 + 1);
	m_pServerIP.SetAddress((BYTE) _tstoi(strServerIP.Mid(0, nServer1)),
		(BYTE) _tstoi(strServerIP.Mid(nServer1 + 1, nServer2 - nServer1)),
		(BYTE) _tstoi(strServerIP.Mid(nServer2 + 1, nServer3 - nServer2)),
		(BYTE) _tstoi(strServerIP.Mid(nServer3 + 1, strServerIP.GetLength() - nServer3)));

	CString strServerPort;
	strServerPort.Format(_T("%d"), theApp.GetInt(_T("ServerPort"), 0));
	m_pServerPort.SetWindowText(strServerPort);
	m_pServerPort.SetLimitText(5);

	CString strClientIP = theApp.GetString(_T("ClientIP"), _T("127.0.0.1"));
	if (strClientIP.IsEmpty()) strClientIP = _T("127.0.0.1");
	const int nClient1 = strClientIP.Find(_T('.'), 0);
	const int nClient2 = strClientIP.Find(_T('.'), nClient1 + 1);
	const int nClient3 = strClientIP.Find(_T('.'), nClient2 + 1);
	m_pClientIP.SetAddress((BYTE) _tstoi(strClientIP.Mid(0, nClient1)),
		(BYTE) _tstoi(strClientIP.Mid(nClient1 + 1, nClient2 - nClient1)),
		(BYTE) _tstoi(strClientIP.Mid(nClient2 + 1, nClient3 - nClient2)),
		(BYTE) _tstoi(strClientIP.Mid(nClient3 + 1, strClientIP.GetLength() - nClient3)));

	CString strClientPort;
	strClientPort.Format(_T("%d"), theApp.GetInt(_T("ClientPort"), 0));
	m_pClientPort.SetWindowText(strClientPort);
	m_pClientPort.SetLimitText(5);

	OnSelchangeConnection();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CConfigureDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
}

void CConfigureDlg::OnOK()
{
	theApp.WriteInt(_T("Connection"), m_pConnection.GetCurSel());

	CString strSerialPort;
	if (m_pSerialPortNames.GetCount() > 0)
		m_pSerialPortNames.GetLBText(m_pSerialPortNames.GetCurSel(), strSerialPort);
	theApp.WriteString(_T("SerialName"), strSerialPort);

	switch (m_pBaudRate.GetCurSel())
	{
		case 0:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_110);
			break;
		}
		case 1:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_300);
			break;
		}
		case 2:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_600);
			break;
		}
		case 3:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_1200);
			break;
		}
		case 4:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_2400);
			break;
		}
		case 5:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_4800);
			break;
		}
		case 6:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_9600);
			break;
		}
		case 7:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_14400);
			break;
		}
		case 8:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_19200);
			break;
		}
		case 9:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_38400);
			break;
		}
		case 10:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_57600);
			break;
		}
		case 11:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_115200);
			break;
		}
		case 12:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_128000);
			break;
		}
		case 13:
		{
			theApp.WriteInt(_T("BaudRate"), CBR_256000);
			break;
		}
	}

	switch (m_pDataBits.GetCurSel())
	{
		case 0:
		{
			theApp.WriteInt(_T("DataBits"), 7);
			break;
		}
		case 1:
		{
			theApp.WriteInt(_T("DataBits"), 8);
			break;
		}
	}

	switch (m_pParity.GetCurSel())
	{
		case 0:
		{
			theApp.WriteInt(_T("Parity"), CSerialPort::NoParity);
			break;
		}
		case 1:
		{
			theApp.WriteInt(_T("Parity"), CSerialPort::OddParity);
			break;
		}
		case 2:
		{
			theApp.WriteInt(_T("Parity"), CSerialPort::EvenParity);
			break;
		}
		case 3:
		{
			theApp.WriteInt(_T("Parity"), CSerialPort::MarkParity);
			break;
		}
		case 4:
		{
			theApp.WriteInt(_T("Parity"), CSerialPort::SpaceParity);
			break;
		}
	}

	switch (m_pStopBits.GetCurSel())
	{
		case 0:
		{
			theApp.WriteInt(_T("StopBits"), CSerialPort::OneStopBit);
			break;
		}
		case 1:
		{
			theApp.WriteInt(_T("StopBits"), CSerialPort::OnePointFiveStopBits);
			break;
		}
		case 2:
		{
			theApp.WriteInt(_T("StopBits"), CSerialPort::TwoStopBits);
			break;
		}
	}

	switch (m_pFlowControl.GetCurSel())
	{
		case 0:
		{
			theApp.WriteInt(_T("FlowControl"), CSerialPort::NoFlowControl);
			break;
		}
		case 1:
		{
			theApp.WriteInt(_T("FlowControl"), CSerialPort::CtsRtsFlowControl);
			break;
		}
		case 2:
		{
			theApp.WriteInt(_T("FlowControl"), CSerialPort::CtsDtrFlowControl);
			break;
		}
		case 3:
		{
			theApp.WriteInt(_T("FlowControl"), CSerialPort::DsrRtsFlowControl);
			break;
		}
		case 4:
		{
			theApp.WriteInt(_T("FlowControl"), CSerialPort::DsrDtrFlowControl);
			break;
		}
		case 5:
		{
			theApp.WriteInt(_T("FlowControl"), CSerialPort::XonXoffFlowControl);
			break;
		}
	}

	theApp.WriteInt(_T("SocketType"), m_pSocketType.GetCurSel());

	CString strServerIP;
	BYTE nServer1, nServer2, nServer3, nServer4;
	m_pServerIP.GetAddress(nServer1, nServer2, nServer3, nServer4);
	strServerIP.Format(_T("%d.%d.%d.%d"), nServer1, nServer2, nServer3, nServer4);
	theApp.WriteString(_T("ServerIP"), strServerIP);

	CString strServerPort;
	m_pServerPort.GetWindowText(strServerPort);
	theApp.WriteInt(_T("ServerPort"), _tstoi(strServerPort));

	CString strClientIP;
	BYTE nClient1, nClient2, nClient3, nClient4;
	m_pClientIP.GetAddress(nClient1, nClient2, nClient3, nClient4);
	strClientIP.Format(_T("%d.%d.%d.%d"), nClient1, nClient2, nClient3, nClient4);
	theApp.WriteString(_T("ClientIP"), strClientIP);

	CString strClientPort;
	m_pClientPort.GetWindowText(strClientPort);
	theApp.WriteInt(_T("ClientPort"), _tstoi(strClientPort));

	CDialogEx::OnOK();
}


void CConfigureDlg::OnSelchangeConnection()
{
	m_pSerialPortNames.EnableWindow(m_pConnection.GetCurSel() == 0);
	m_pBaudRate.EnableWindow(m_pConnection.GetCurSel() == 0);
	m_pDataBits.EnableWindow(m_pConnection.GetCurSel() == 0);
	m_pParity.EnableWindow(m_pConnection.GetCurSel() == 0);
	m_pStopBits.EnableWindow(m_pConnection.GetCurSel() == 0);
	m_pFlowControl.EnableWindow(m_pConnection.GetCurSel() == 0);

	m_pSocketType.EnableWindow(m_pConnection.GetCurSel() != 0);
	m_pServerIP.EnableWindow((m_pConnection.GetCurSel() != 0) && (m_pSocketType.GetCurSel() != 0));
	m_pServerPort.EnableWindow((m_pConnection.GetCurSel() != 0) && (m_pSocketType.GetCurSel() != 0));
	m_pClientIP.EnableWindow(m_pConnection.GetCurSel() != 0);
	m_pClientPort.EnableWindow(m_pConnection.GetCurSel() != 0);
}

void CConfigureDlg::OnSelchangeSocketType()
{
	m_pServerIP.EnableWindow((m_pConnection.GetCurSel() != 0) && (m_pSocketType.GetCurSel() != 0));
	m_pServerPort.EnableWindow((m_pConnection.GetCurSel() != 0) && (m_pSocketType.GetCurSel() != 0));
}
